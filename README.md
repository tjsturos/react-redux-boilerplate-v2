<h1>Expensify Application</h1>

<h2>Downloading Project</h2>
//TODO

<h2>To Develop Locally</h2>
To run this project on your local client, run the command "npm run-script dev-server" or "yarn dev-server".  

<h2>To Build Distribution Files and Run Server</h2>
<h3>Building Distribution Files</h3>
<p>To build the distribution files, run "npm run-script build" or "yarn build".</p>
<h3>Run Server (Build Required, see "Building Distribution Files")</h3>
<p>To run this with the built bundle.js distribution file, run "npm start" or "yart start".</p>