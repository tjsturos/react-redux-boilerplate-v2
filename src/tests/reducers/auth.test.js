import authReducer from '../../reducers/auth'

test('should return uid on login', () => {
    const uid =  '123dadabd123';
    const action = {
        type: "LOGIN",
        uid
    }

    const state = authReducer({}, action);
    expect(state).toEqual({ uid })
})

test('should return nothing on logout', () => {
    
    const action = {
        type: "LOGOUT"
    }

    const state = authReducer({ uid: 'anything' }, action);
    expect(state).toEqual({})
})