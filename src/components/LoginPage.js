import React from 'react';
import { connect } from 'react-redux';
import { startLogin } from '../actions/auth';
 
export const LoginPage = ({ startLogin }) => (
    <div className="box-layout">
        <div className="box-layout__box">
            <h1 className="box-layout__title">React-Redux Boilerplate App (LoginPage.js)</h1>
            <p>Get something done.</p>
            <button className="button button__default" onClick={startLogin}>Login with Google</button>
        </div>
        
    </div>
)

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
})

export default connect(undefined, mapDispatchToProps)(LoginPage);